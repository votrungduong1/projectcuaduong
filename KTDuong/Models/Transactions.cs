﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KTDuong.Models
{
    public class Transactions
    {
        public int TransactionalID { get; set; }
        [ForeignKey("Employees")]
        public int EmployeeID { get; set;}
        [ForeignKey("Customer")]
        public int CustomerID { get; set;}
        public string Name { get; set;}
        public Reports? Reports { get; set;}
        public Logs? Logs { get; set;}
        public ICollection<Employees> Employees { get; set; }
    }
}
