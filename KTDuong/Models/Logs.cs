﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KTDuong.Models
{
    public class Logs
    {
        public int ID { get; set; }
        [ForeignKey("Transactions")]
        public int TransactionalID { get; set; }
        public string Logindate { get; set; }
        public string time { get; set; }
        public Reports? Reports { get; set; }

        public ICollection<Transactions> Transactions { get; set; }

    }
}
