﻿namespace KTDuong.Models
{
    public class Customer
    {
        public int ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Contactandaddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public Accounts Accounts { get; set; }
    }
}
