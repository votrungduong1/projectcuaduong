﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KTDuong.Models
{
    public class Reports
    {
        public int ID { get; set; }
        [ForeignKey("Accounts")]
        public int AccountID { get; set; }
        [ForeignKey("Logs")]
        public int LogsID { get; set; }
        [ForeignKey("Transactions")]
        public int TransactionalID { get; set; }
        public string Reportname { get; set; }
        public string Reportdate { get; set; }
        public ICollection<Logs> Logs { get; set; }
        public ICollection<Transactions> Transactions { get; set; }
        public ICollection<Accounts> Accounts { get; set; }
    }
}
