﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KTDuong.Models
{
    public class Accounts
    {
        public int ID { get; set; }
        [ForeignKey("Customer")]
        public int CustomerID { get; set; }
        public string AccountsName { get; set; }
        public Reports? Reports { get; set; }
        public ICollection<Customer> Customers { get; set; }

    }
}
